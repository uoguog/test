package com.data;

/**
 * 请添加描述信息。
 *
 * @author 80296858
 * @version 1.0
 * @date 2020/9/7
 */
public class NumberTest {
    private int value;
    public NumberTest(int initialValue) {
        value = initialValue;
    }

    public int update(int newValue) {
        this.value = newValue;
        return this.value;
    }

    public int getValue() {
        return this.value;
    }
}
