package com.data;

import lombok.Data;

/**
 * 请添加描述信息。
 *
 * @author 80296858
 * @version 1.0
 * @date 2020/9/4
 */
@Data
public class Message {
    private String orderId;
    private Long userId;
    private String content;
}
