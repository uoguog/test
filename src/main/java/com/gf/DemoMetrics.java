package com.gf;

import com.data.NumberTest;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.binder.MeterBinder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.Timer;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 在metrics下增加一些指标信息
 *
 * @author 80296858
 * @version 1.0
 * @date 2020/9/2
 */
@Slf4j
@Component
public class DemoMetrics implements MeterBinder {
    private static final Random random = new Random();
    AtomicInteger count = new AtomicInteger(0);
    NumberTest numberTest = new NumberTest(0);
    @Override
    public void bindTo(MeterRegistry meterRegistry) {
        Gauge.builder("demo.count", numberTest, NumberTest::getValue)
                .tags("host", "localhost")
                .description("demo of custom meter binder")
                .register(meterRegistry);
        Gauge.builder("demo.count", numberTest, NumberTest::getValue)
                .tags("host", "localhost_test")
                .description("demo of custom meter binder")
                .register(meterRegistry);
    }
    @Scheduled(fixedDelay = 5000)
    //@Timed(value = "custom.task.time", extraTags = {"name", "自定义定时任务"}, description = "自定义定时任务监控")
    public void customSchedule() throws InterruptedException {
        numberTest.update(12);
        Thread.sleep(random.nextInt(5000));
        log.info("test time");
    }

}
