package com.task;

import com.data.NumberTest;
import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * 请添加描述信息。
 *
 * @author 80296858
 * @version 1.0
 * @date 2020/9/7
 */
@Slf4j
@Component
public class CustomScheduleTask  {
    private static final Random random = new Random();

//    @Autowired
//    private MeterRegistry meterRegistry;

    static SimpleMeterRegistry meterRegistry = new SimpleMeterRegistry();

    static NumberTest numberTest = new NumberTest(0);

    static {
        Gauge gauge = Gauge.builder("task.failed.timeout", numberTest, NumberTest::getValue)
                .tags("taskFailed", "thirty minutes")
                .register(meterRegistry);
    }


    //@Scheduled(fixedDelay = 5000)
    //@Timed(value = "custom.task.time", extraTags = {"name", "自定义定时任务"}, description = "自定义定时任务监控")
    public void customSchedule() throws InterruptedException {
        numberTest.update(10);
        Thread.sleep(random.nextInt(5000));
        log.info("定时任务执行完成");
    }
}
