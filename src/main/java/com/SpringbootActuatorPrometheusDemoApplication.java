package com;

import com.gf.DemoMetrics;
import com.gf.FeaturesEndpoint;
import com.task.CustomScheduleTask;
import io.micrometer.core.aop.TimedAspect;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Metrics;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * @author 80296858
 */
@EnableScheduling
@SpringBootApplication
@RestController
public class SpringbootActuatorPrometheusDemoApplication {

    public static void main(String[] args) throws InterruptedException {

        ApplicationContext ctx = SpringApplication.run(SpringbootActuatorPrometheusDemoApplication.class, args);

        System.out.println("Inspect the beans provides by Spring Boot");

//        CustomScheduleTask customScheduleTask = new CustomScheduleTask();
//        customScheduleTask.customSchedule();
//        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
//
//        cachedThreadPool.execute(new Runnable() {
//            @SneakyThrows
//            @Override
//            public void run() {
//                customScheduleTask.customSchedule();
//            }
//        });


        String[] beadNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beadNames);
        for (String beanName : beadNames) {
            System.out.println(beanName);
        }
    }

    @RequestMapping(value = "/hello")
    public String  sayHello() {
        System.out.print("Hello");
        return "ok";
    }

/*    @Bean
    MeterRegistryCustomizer<MeterRegistry> metricsCommonTags() {
        return registry -> registry.config().commonTags("region", "us-east-1");
    }*/
//    @Bean
//    public TimedAspect timedAspect(MeterRegistry registry) {
//        return new TimedAspect(registry);
//    }

//    @Bean
//    public DemoMetrics demoMetrics(){
//        return new DemoMetrics();
//    }

    @Autowired
    FeaturesEndpoint featuresEndpoint;

    @RequestMapping(value = "/feature/operation")
    public void setFeaturesEndpoint(@RequestParam("name") String name, @RequestParam("feature") String feature) {
        featuresEndpoint.configureFeature(name, feature);
    }

/*    List<InfoContributor> info = new List<InfoContributor>();
    @Bean
    public InfoWebEndpointExtension infoWebEndpointExtension(){
        return new InfoWebEndpointExtension(new InfoEndpoint(info));
    }*/
/*
    @Bean
    MeterRegistryCustomizer<MeterRegistry> metricsCommonTags() {
        return registry -> registry.config().commonTags("application", "springboot-actuator-prometheus-test");
    }
*/
}
